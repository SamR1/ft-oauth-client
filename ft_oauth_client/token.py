from .models import OAuth2Token, db


def save_token(token_dict: dict) -> None:
    # delete previous token
    OAuth2Token.query.delete()
    db.session.flush()

    token = OAuth2Token(
        access_token=token_dict["access_token"],
        refresh_token=token_dict["refresh_token"],
        scope="".join(token_dict["scope"]),
        token_type=token_dict["token_type"],
        expires_in=token_dict["expires_in"],
        expires_at=int(token_dict["expires_at"]),
    )
    db.session.add(token)
    db.session.commit()


def get_token() -> dict:
    token = OAuth2Token.query.first()
    return {
        "access_token": token.access_token,
        "scope": token.scope.split(" "),
        "expires_at": float(token.expires_at),
        "expires_in": token.expires_in,
        "refresh_token": token.refresh_token,
        "token_type": token.token_type,
    }
