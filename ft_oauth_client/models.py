from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import DeclarativeMeta

db = SQLAlchemy()
BaseModel: DeclarativeMeta = db.Model


class OAuth2Token(BaseModel):
    __tablename__ = "oauth2_token"

    id = db.Column(db.Integer, primary_key=True)
    access_token = db.Column(
        db.String(255), index=True, unique=True, nullable=False
    )
    refresh_token = db.Column(db.String(255), index=True)
    scope = db.Column(db.Text, default="")
    token_type = db.Column(db.String(40))
    expires_in = db.Column(db.Integer, nullable=False, default=0)
    expires_at = db.Column(db.Integer, nullable=False, default=0)
