from requests_oauthlib import OAuth2Session

from ft_oauth_client.config import Config
from ft_oauth_client.token import get_token, save_token


def get_fittrackee_session(config: Config) -> OAuth2Session:
    return OAuth2Session(
        config.CLIENT.id,
        token=get_token(),
        auto_refresh_url=config.TOKEN_URL,
        auto_refresh_kwargs=config.CLIENT.extra(),
        scope=config.CLIENT.scope,
        token_updater=save_token,
    )
