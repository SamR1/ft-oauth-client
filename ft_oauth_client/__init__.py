from flask import Flask
from werkzeug.middleware.proxy_fix import ProxyFix

from .config import Config
from .models import db

config = Config()


def create_app() -> Flask:
    app = Flask(__name__)
    app.config.from_object(config)
    app.wsgi_app = ProxyFix(app.wsgi_app)  # type: ignore

    @app.before_first_request
    def create_tables() -> None:
        db.create_all()

    db.init_app(app)

    from .routes import bp

    app.register_blueprint(bp, url_prefix="")

    return app
