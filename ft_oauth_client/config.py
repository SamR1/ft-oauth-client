import os
from dataclasses import dataclass

import yaml

SECRET_KEY = os.getenv("SECRET_KEY")
GPX_DIRECTORY = os.getenv("GPX_DIRECTORY")
CONFIG_FILE = os.getenv("CONFIG_FILE")

if not SECRET_KEY or not GPX_DIRECTORY or not CONFIG_FILE:
    raise Exception(
        "Invalid config, please check values in Makefile.local.config."
    )


@dataclass()
class Client:
    id: str
    secret: str
    scope: str

    def extra(self) -> dict:
        return {
            "client_id": self.id,
            "client_secret": self.secret,
        }


def get_config() -> dict:
    config_file = os.getenv("CONFIG_FILE", "")
    check_config = f", please check values in {config_file}."

    with open(config_file, "r", encoding="utf-8") as stream:
        config = yaml.safe_load(stream)
        if not config:
            raise Exception(f"No config{check_config}")

        if not set(config.get("client", {}).keys()) == {
            "id",
            "secret",
            "scope",
        }:
            raise Exception(f"Invalid client config{check_config}")

        if not config.get("fittrackee_host"):
            raise Exception(f"Invalid fittrackee host{check_config}")

        try:
            config.get("matching_labels").keys()
        except AttributeError:
            raise Exception(f"Invalid sports labels config{check_config}")

        return config


def create_gpx_directories(directory: str) -> None:
    if directory:
        os.makedirs(os.path.join(directory, "done"), exist_ok=True)
        os.makedirs(os.path.join(directory, "error"), exist_ok=True)


yaml_config = get_config()
host = yaml_config.get("fittrackee_host")
create_gpx_directories(GPX_DIRECTORY)


class Config:

    SECRET_KEY = SECRET_KEY

    AUTHORIZATION_BASE_URL = f"https://{host}/profile/apps/authorize"
    BASE_URL = f"https://{host}/api"
    TOKEN_URL = f"{BASE_URL}/oauth/token"
    CLIENT = Client(
        id=yaml_config.get("client", {}).get("id"),
        secret=yaml_config.get("client", {}).get("secret"),
        scope=yaml_config.get("client", {}).get("scope"),
    )

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "sqlite:///db.sqlite"

    GPX_DIRECTORY = GPX_DIRECTORY
    SPORTS_CONFIG = yaml_config.get("matching_labels")
