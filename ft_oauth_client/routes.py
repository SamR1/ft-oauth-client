"""
Web App Example of OAuth 2 web application flow for FitTrackee

Source:
https://requests-oauthlib.readthedocs.io/en/latest/examples/real_world_example.html
"""
from flask import Blueprint, redirect, request, session, url_for
from requests_oauthlib import OAuth2Session
from werkzeug.wrappers.response import Response

from .config import Config
from .session import get_fittrackee_session
from .token import save_token

config = Config()

bp = Blueprint("main", __name__)


@bp.route("/")
def authorize() -> Response:
    """Step 1: User Authorization.

    Redirect the user/resource owner to the OAuth provider
    using a URL with a few key OAuth parameters.
    """
    fittrackee = OAuth2Session(config.CLIENT.id, scope=config.CLIENT.scope)
    authorization_url, state = fittrackee.authorization_url(
        config.AUTHORIZATION_BASE_URL
    )

    # State is used to prevent CSRF, keep this for later.
    session["oauth_state"] = state
    return redirect(authorization_url)


# Step 2: User authorization, this happens on the provider.
@bp.route("/callback", methods=["GET"])
def callback() -> Response:
    """Step 3: Retrieving an access token.

    The user has been redirected back from the provider to your registered
    callback URL. With this redirection comes an authorization code included
    in the redirect URL. We will use that to obtain an access token.
    """

    fittrackee = OAuth2Session(config.CLIENT.id, state=session["oauth_state"])
    token = fittrackee.fetch_token(
        config.TOKEN_URL,
        include_client_id=True,
        client_secret=config.CLIENT.secret,
        authorization_response=request.url,
    )

    # At this point you can fetch protected resources but lets save
    # the token and show how this is done from a persisted token
    # in /profile.
    save_token(token)

    return redirect(url_for(".profile"))


@bp.route("/profile", methods=["GET"])
def profile() -> dict:
    """Fetching a protected resource using an OAuth 2 token."""
    fittrackee = get_fittrackee_session(config)
    return fittrackee.get(f"{config.BASE_URL}/auth/profile").json()
