include Makefile.config
include Makefile.custom.config

check: lint type-check

clean:
	rm -rf .mypy_cache
	rm -rf .pytest_cache

install: venv
	$(PIP) install -r requirements.txt

install-dev: venv
	$(PIP) install -r requirements-dev.txt

lint:
	$(PYTEST) --flake8 --isort --black -m "flake8 or isort or black" ft_oauth_client

lint-fix:
	$(BLACK) ft_oauth_client import_gpx.py app.py

watch-files:
	$(PYTHON) fittrackee_importer.py

serve:
	$(FLASK) run --with-threads -h $(HOST) -p $(PORT)

type-check:
	echo 'Running mypy...'
	$(MYPY) ft_oauth_client

venv:
	test -d $(VENV) || $(PYTHON_VERSION) -m venv $(VENV)