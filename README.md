# ft-oauth-client

Minimal example for a **FitTrackee** OAuth 2.0 Client (for instance, a gpx/kml importer)  
(needs **FitTrackee** v0.7+)

(for PKCE support, see branch [PKCE_support](https://codeberg.org/SamR1/ft-oauth-client/src/branch/PKCE_support))

## Installation

* clone this repo and install Python dependencies
```shell
$ make install
```

* create an application (oauth2 client) in **FitTrackee**

[<img src="screenshots/1-create-aplication.png" width="400px">](screenshots/1-create-aplication.png)

[<img src="screenshots/2-application-created.png" width="400px">](screenshots/2-application-created.png)

* create the client config file and add **FitTrackee** instance URL, the client credentials and the matching label for each sport.
```shell
$ cp config.yml.example config.yml
$ nano config.yml
```

* create **Makefile.custom.config** and update environnement variables
```shell
$ cp Makefile.custom.config.example Makefile.custom.config
$ nano Makefile.custom.config
```

For tests without HTTPS, set the following environment variable:

```
export OAUTHLIB_INSECURE_TRANSPORT=1
```

## Client Authorization

* start the server and open http://0.0.0.0:5000:  
```shell
$ make serve
```

* authorize the application on FitTrackee

[<img src="screenshots/3-authorize-application.png" width="400px">](screenshots/3-authorize-application.png)

After a successful authorization, you should access your profile info (**FitTrackee** endpoint `/auth/profile`).  
A valid token is stored in SQLite data.


## GPX Importer

The importer monitors files created into a configured directory.  
It supports GPX and KML files (only files with offset in date time for KML file). KML files are converted into GPX format before import.

**Note**: it's a minimal example, just to test OAuth2. Some files may not be importable.

Run the script and add files into the configured directory:

```shell
$ make watch-files 

Starting watch process...

IMPORTER  - Importing file '2022-05-07 16_38.gpx'...
IMPORTER  - > an error has occurred when get sport id for file '2022-05-07 16_38.gpx': Exception no matching label.
CONVERTER - Converting file '2022-06-04 11_16_running.kml' to gpx format...
CONVERTER - > conversion done for '2022-06-04 11_16_running.kml'.
IMPORTER  - Importing file '2022-06-04 11_16_running.gpx'...
IMPORTER  - > import done for file '2022-06-04 11_16_running.gpx'.
IMPORTER  - Importing file '2022-06-12_cycling.gpx'...
IMPORTER  - > import done for file '2022-06-12_cycling.gpx'.
```

---
sources:
- [Requests-OAuthlib - Web App Example of OAuth 2 web application flow](https://requests-oauthlib.readthedocs.io/en/latest/examples/real_world_example.html)
