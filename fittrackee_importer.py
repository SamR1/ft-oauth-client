import logging
import time
import os
import shutil
from datetime import datetime
from pathlib import Path

import gpxpy.gpx
import xmltodict
from watchdog.observers import Observer
from watchdog.events import FileCreatedEvent, PatternMatchingEventHandler

from ft_oauth_client import create_app
from ft_oauth_client.config import Config
from ft_oauth_client.session import get_fittrackee_session

logging.basicConfig(
    filename=os.getenv("LOG_FILE"),
    format="%(asctime)s - %(name)s - %(levelname)s - " "%(message)s",
    datefmt="%Y/%m/%d %H:%M:%S",
)
logger = logging.getLogger("fittrackee_import")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
logger.addHandler(handler)

config = Config()
app = create_app()
WORKOUTS_POST_URL = f"{config.BASE_URL}/workouts"


class KML2GPX:
    """
    Example for KML files generated with OpenTracks (with offset in date time)
    """

    def __init__(self, file_path: str) -> None:
        self.kml_path = file_path
        self.gpx_path = file_path.replace("kml", "gpx")
        self.gpx = gpxpy.gpx.GPX()
        self.gpx_track = gpxpy.gpx.GPXTrack()
        self.gpx.tracks.append(self.gpx_track)

    def parse(self) -> None:
        with open(self.kml_path) as file:
            kml_dict = xmltodict.parse(file.read())
            doc = kml_dict["kml"]["Document"]
            kml_tracks = (
                doc.get("Placemark", {})
                .get("gx:MultiTrack", {})
                .get("gx:Track")
            )

            if isinstance(kml_tracks, dict):
                kml_tracks = [kml_tracks]

            for kml_track in kml_tracks:
                gpx_segment = gpxpy.gpx.GPXTrackSegment()
                self.gpx_track.segments.append(gpx_segment)
                coords = kml_track.get("gx:coord", [])
                for index, date in enumerate(kml_track.get("when", [])):
                    if not coords[index]:
                        continue
                    lon, lat, ele = coords[index].split()
                    gpx_segment.points.append(
                        gpxpy.gpx.GPXTrackPoint(
                            longitude=lon,
                            latitude=lat,
                            elevation=ele,
                            time=datetime.strptime(
                                date, "%Y-%m-%dT%H:%M:%S.%f%z"
                            ),
                        )
                    )

    def write(self) -> None:
        with open(self.gpx_path, "w") as f:
            f.write(self.gpx.to_xml())


def move_file(file_path: str, is_errored: bool) -> None:
    filename = Path(file_path).name
    shutil.move(
        file_path,
        os.path.join(
            config.GPX_DIRECTORY, "error" if is_errored else "done", filename
        ),
    )


def get_sport_id(filename: str) -> int:
    for matching_label in config.SPORTS_CONFIG.keys():
        if matching_label in filename:
            return config.SPORTS_CONFIG.get(matching_label)
    raise Exception(f"no matching label")


def convert_kml(event: FileCreatedEvent) -> None:
    is_errored = False
    filename = Path(event.src_path).name
    logger.info(f"CONVERTER - Converting file '{filename}' to gpx format...")

    try:
        converter = KML2GPX(event.src_path)
        converter.parse()
        converter.write()
    except Exception as e:
        is_errored = True
        logger.error(
            "CONVERTER - > an error has occurred when "
            f"converting '{filename}' to gpx from: {e.__class__.__name__} {e}."
        )
    if not is_errored:
        logger.info(f"CONVERTER - > conversion done for '{filename}'.")

    move_file(event.src_path, is_errored=is_errored)


def import_gpx(event: FileCreatedEvent) -> None:
    file_path = event.src_path
    filename = Path(file_path).name
    with app.app_context():
        fittrackee = get_fittrackee_session(config)
        logger.info(f"IMPORTER  - Importing file '{filename}'...")

        try:
            sport_id = get_sport_id(filename)
        except Exception as e:
            move_file(file_path, is_errored=True)
            logger.error(
                "IMPORTER  - > an error has occurred when get sport id"
                f" for file '{filename}': {e.__class__.__name__} {e}."
            )
            return

        file = open(file_path, "r")
        r = fittrackee.post(
            WORKOUTS_POST_URL,
            data=[("data", f'{{"sport_id": {sport_id}}}')],
            files=[("file", file)],
            headers=dict(
                content_type="multipart/form-data",
            ),
        )
        if r.status_code == 201:
            move_file(file_path, is_errored=False)
            logger.info(f"IMPORTER  - > import done for file '{filename}'.")
        else:
            move_file(file_path, is_errored=True)
            try:
                logger.error(
                    "IMPORTER  - > an error has occurred when importing file"
                    f" '{filename}': {r.json()}."
                )
            except Exception:
                logger.error(
                    "IMPORTER  - > an error has occurred when importing file"
                    f" '{filename}', check your FitTrackee instance."
                )


if __name__ == "__main__":
    observer = Observer()

    gpx_event_handler = PatternMatchingEventHandler(patterns=["*.gpx"])
    gpx_event_handler.on_created = import_gpx
    observer.schedule(
        event_handler=gpx_event_handler, path=config.GPX_DIRECTORY
    )

    kml_event_handler = PatternMatchingEventHandler(patterns=["*.kml"])
    kml_event_handler.on_created = convert_kml
    observer.schedule(
        event_handler=kml_event_handler, path=config.GPX_DIRECTORY
    )

    logger.info(f"Starting watch process...\n")
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
        observer.join()
